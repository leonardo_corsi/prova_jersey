package it.formatemp.indra.jersey_ex;

import javax.ws.rs.ApplicationPath;

import org.glassfish.jersey.server.ResourceConfig;

//root path del nostro servizio rest
@ApplicationPath("api")
public class ServiceApplication extends ResourceConfig {
	/**
	 * costruttore
	 */
	public ServiceApplication() {
		packages("it.formatemp.indra.jersey_ex");
	}

}
