package it.formatemp.indra.jersey_ex;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;

/**
 * Questo lo usiamo come path di test; giusto per vedere se funziona il servizio
 * (non confondere con test di unit� :)
 * @author Leonardo
 *
 */
@Path("test")
public class TestApi {

	@GET
	public String testDefault() {
		return "Hello! It works!";
	}
	
	@GET
	@Path("{name}")
	public String testParams(@PathParam("name") String nome) {
		return "Ciao "+nome;
	}
	
}
