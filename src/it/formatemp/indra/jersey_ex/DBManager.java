package it.formatemp.indra.jersey_ex;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBManager {
	
	//creare una classe di test sul database
	public static void test() {

		final String pwd = "indra";
	//	System.out.println("Provando a usare pwd " + pwd);
		try {
			Class.forName("org.postgresql.Driver");
			Connection c = DriverManager.getConnection("jdbc:postgresql://localhost:5432/Leonardo", "Leonardo", pwd);
			c.setAutoCommit(true);
			c.close();
		} catch (ClassNotFoundException | SQLException e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			// System.exit(0);
		}
		System.out.println("Opened database successfully");
	}
}
