package it.formatemp.indra.jersey_ex;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Servizio di gestione delgli impiegati
 * @author Leonardo
 *
 */
@Path("impiegati")
public class ImpiegatoService {

	@GET
	@Produces(MediaType.APPLICATION_JSON)//qui forzo la restituzione del solo json. Se il client richiede xml la risposta del server sarà 406
    public  List<Impiegato> test(){
		//DBManager.test();
        List<Impiegato> listaImpiegati = new ArrayList<>(2);//adesso restituiamo tutto in maniera statica
        Impiegato mario = new Impiegato("Mario", "Rossi", "562372", 'm'); mario.setIdDip(3);
        Impiegato maria = new Impiegato("Maria", "Rossi", "562373", 'f'); maria.setIdDip(1);
        listaImpiegati.add(maria);
        listaImpiegati.add(mario);
        return listaImpiegati;
    }
	
	//in base all'header del client restituirà o json o xml!!!
    @GET
    @Path("{id}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
//    public Impiegato getImpiegato(@PathParam("id") int id, @PathParam("matricola") String matricola) {
    public Impiegato getImpiegato(@PathParam("id") int id) {   
        Impiegato mario = new Impiegato("Mario", "Rossi", "562372", 'm'); mario.setIdDip(id);
        return mario;
    }
    
    
    @POST
    public Response add(Impiegato impiegato) throws URISyntaxException {
        long newId = 3;
        return Response.created(new URI("api/impiegati/" + newId)).build();
        //Location:http://localhost:8080/prova_jersey/api/api/impiegati/3
    }
     
    @PUT
    @Path("{id}")
    public Response update(@PathParam("id") long id, Impiegato impiegato) {
        return Response.noContent().build();
    }
     
    @DELETE
    @Path("{id}")
    public Response delete(@PathParam("id") long id) {
        return Response.noContent().build();
    }
    
	
}
