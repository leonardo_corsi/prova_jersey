package it.formatemp.indra.jersey_ex;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

public class Impiegato {
 private String nome;
 private String cognome;
 private String matricola;
 private char sesso;
 private int idDip;
 
 /**
  * default
  */
 public Impiegato() {}
 
 public Impiegato(String nome, String cognome, String matricola, char sesso){
     this.nome = nome;
     this.cognome = cognome;
     this.matricola = matricola;
     this.sesso = sesso;
 }
 
 public void setNome(String nome) {
     this.nome = nome;
 }

 public void setCognome(String cognome) {
     this.cognome = cognome;
 }

 public void setMatricola(String matricola) {
     this.matricola = matricola;
 }

 public void setSesso(char sesso) {
     this.sesso = sesso;
 }
 
 public void setIdDip(int idDip){
     this.idDip = idDip;
 }
 
 @XmlElement
 public String getNome() {
     return nome;
 }
 @XmlElement
 public String getCognome() {
     return cognome;
 }
 @XmlAttribute
 public String getMatricola() {
     return matricola;
 }
 @XmlElement
 public char getSesso() {
     return sesso;
 }
 @XmlElement
 public int getIdDip(){
     return idDip;
 }
 
}
